

// A basic threading example in java
public class Threading01 {

	final static int HOURS = 2000;
	static int dollars = 0;
	
	public static void main(String[] args) {
		
		// You got bills to pay!
		// So you have 2 jobs, one at Mcds and the other at Timmies
		Thread jobTimmies = new Thread("working at Tims") {
			public void run() {
				for (int i = 0; i < HOURS; i++) {					
					dollars = dollars + 1;
//					System.out.println(Thread.currentThread().getName() + 
//							" - Bank Account: " + dollars);
				}
			}
			
		};
		
		Thread jobMcds = new Thread("working at mcds") {
			public void run() {
				for (int i = 0; i < HOURS; i++) {
					dollars = dollars + 1;
//					System.out.println(Thread.currentThread().getName() + 
//							" -  Bank Account: " + dollars);
				}
			}
		};
		
		jobTimmies.start();
		jobMcds.start();
		
		System.out.println("++++ TOTAL MONEY: " + dollars);

	}

}
