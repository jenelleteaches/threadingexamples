// Fixing example 2 using synchronized keyword
public class Threading03 {
	
	final static int HOURS = 10000;
	static int dollars = 0;
	
	// This is putting lock on the code
	public static synchronized void deposit() {
		dollars = dollars + 1;
	}
	
	
	public static void main(String[] args) {
	
		// You got bills to pay!
		// So you have 2 jobs, one at Mcds and the other at Timmies
		Thread jobTimmies = new Thread("working at Tims") {
			public void run() {
				for (int i = 0; i < HOURS; i++) {					
					deposit();
//					System.out.println(Thread.currentThread().getName() + 
//							" - Bank Account: " + dollars);
				}
			}
			
		};
		
		Thread jobMcds = new Thread("working at mcds") {
			public void run() {
				for (int i = 0; i < HOURS; i++) {
					deposit();
//					System.out.println(Thread.currentThread().getName() + 
//							" -  Bank Account: " + dollars);
				}
			}
		};
		
		jobTimmies.start();
		jobMcds.start();

		
		// JOIN - force all threads to finish before 
		// you go to the next section of code (line 44)
		// 	- in other words, wait for execution to complete before
		// 		continuing
		try {
			jobTimmies.join();
			jobMcds.join();
		}
		catch (InterruptedException e) {
			// do nothing
		}
		
		System.out.println("++++ TOTAL MONEY: " + dollars);

	}

}
